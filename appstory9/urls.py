from django.urls import path
from appstory9 import views

app_name = 'appstory9'
urlpatterns = [
    path('', views.index, name='index'),
    path('submit-form/', views.submit_form, name = 'submit_form'),
    path('check-email/', views.check_email, name = 'check_email')
]