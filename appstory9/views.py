from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json
import requests
from .forms import * 
from .models import Status

# Create your views here.
def index(request):
    form = Register_Form()
    return render(request, 'landing.html', {"form" : form})

def submit_form(request):
    if request.method == 'POST':
        nama = request.POST.get('nama', None)
        email = request.POST.get('email', None)
        pasword = request.POST.get('pasword', None)
        Status.objects.create(nama = nama, email = email, pasword = pasword)
    data = {
        "Register_berhasil" : True
    } 
    return JsonResponse(data)

def check_email(request):
    email = request.POST.get('email', None)
    valid = True
    if "@" not in email :
        valid = False
    data = {
        'sudah_ada' : Status.objects.filter(email__iexact = email).exists(),
        'valid': valid
    }
    return JsonResponse(data)
