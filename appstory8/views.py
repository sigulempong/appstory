from django.shortcuts import render

def index(request):
    context = {
        'nama'  : 'Bricen Simamora',
        'npm'   : '1706043935',
        'study'   : 'Fakultas Ilmu Komputer/SI Universitas Indonesia'
    }
    return render(request, 'appstory8/profile_page.html', context)
