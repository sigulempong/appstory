from django.urls import path
from appstory8 import views

urlpatterns = [
    path('', views.index, name='index'),
]
